#ifndef _CAL_INFO_T_H_
#define _CAL_INFO_T_H_

class cal_info_t {
public:
  double offset;
  double slope;
};

#endif
