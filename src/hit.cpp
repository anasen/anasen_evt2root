#include <stdint.h>
#include <vector>
#include "hit.h"

hits_t::hits_t() {
  maxMult = 0;
  reserve(1);
  Reset();
}

hits_t::hits_t(uint64_t s) {
  maxMult = 0;
  reserve(s);
  Reset();
}


size_t hits_t::size() {
  return len;
}

void hits_t::operator=(const hits_t& a) {
  if (a.len > this->maxMult) {
    this->maxMult = a.len;
  }
  this->reserve(this->maxMult);
  this->len = a.len;
  this->sourceid = a.sourceid;
  this->crateid = a.crateid;
  this->slotid = a.slotid;
  this->chanid = a.chanid;
  this->detid = a.detid;
  this->detch = a.detch;
  this->rawval = a.rawval;
  this->value = a.value;
  this->energy = a.energy;
  this->time = a.time;
  this->trace = a.trace;
}

void hits_t::Reset() {
  len=0;
}

// These need to be resize, not reserve, because of the way
// hits_t::push_back works
// TODO: we can change push_back and then change this, if we want
void hits_t::reserve(size_t s) {
  if (s > maxMult) {
    maxMult = s;
    sourceid.resize(s);
    slotid.resize(s);
    crateid.resize(s);
    chanid.resize(s);
    detid.resize(s);
    detch.resize(s);
    rawval.resize(s);
    value.resize(s);
    energy.resize(s);
    time.resize(s);
    trace.resize(s);
  }
}

void hits_t::set_hit(size_t i, single_hit_t h) {
  sourceid[i] = h.sourceid;
  slotid[i] = h.slotid;
  crateid[i] = h.crateid;
  chanid[i] = h.chanid;
  detid[i] = h.detid;
  detch[i] = h.detch;
  rawval[i] = h.rawval;
  value[i] = h.value;
  energy[i] = h.energy;
  time[i] = h.time;
  trace[i] = h.trace;
}

void hits_t::push_back(single_hit_t h) {
  if (len >= maxMult) {
    reserve(len+1);
  }

  set_hit(len, h);
  ++len;
}

single_hit_t hits_t::get_hit(size_t i) {
  return single_hit_t(sourceid[i],
                      slotid[i],
                      crateid[i],
                      chanid[i],
                      detid[i],
                      detch[i],
                      rawval[i],
                      value[i],
                      energy[i],
                      time[i],
                      trace[i]);
}

size_t hits_t::get_maxMult() {
  return maxMult;
}

uint64_t* hits_t::get_len_ptr() {
  return &len;
}

uint64_t* hits_t::get_sourceid_ptr() {
  return sourceid.data();
}

uint64_t* hits_t::get_crateid_ptr() {
  return crateid.data();
}

uint64_t* hits_t::get_slotid_ptr() {
  return slotid.data();
}

uint64_t* hits_t::get_chanid_ptr() {
  return chanid.data();
}

uint64_t* hits_t::get_rawval_ptr() {
  return rawval.data();
}

double* hits_t::get_time_ptr() {
  return time.data();
}

uint64_t* hits_t::get_detid_ptr() {
  return detid.data();
}

uint64_t* hits_t::get_detch_ptr() {
  return detch.data();
}

uint64_t* hits_t::get_value_ptr() {
  return value.data();
}

double* hits_t::get_energy_ptr() {
  return energy.data();
}

std::vector<uint16_t>* hits_t::get_trace_ptr() {
  return trace.data();
}

single_hit_t::single_hit_t() {
}

single_hit_t::single_hit_t(uint64_t so,
                           uint64_t cr,
                           uint64_t sl,
                           uint64_t ch,
                           uint64_t di,
                           uint64_t dc,
                           uint64_t rv,
                           uint64_t v,
                           double   en,
                           double   t) {
  sourceid = so;
  crateid = cr;
  slotid = sl;
  chanid = ch;
  detid = di;
  detch = dc;
  rawval = rv;
  value = v;
  time = t;
  energy = en;
}

single_hit_t::single_hit_t(uint64_t so,
                           uint64_t cr,
                           uint64_t sl,
                           uint64_t ch,
                           uint64_t di,
                           uint64_t dc,
                           uint64_t rv,
                           uint64_t v,
                           double   en,
                           double   t,
                           const std::vector<uint16_t>& tr) {
  sourceid = so;
  crateid = cr;
  slotid = sl;
  chanid = ch;
  detid = di;
  detch = dc;
  rawval = rv;
  value = v;
  energy = en;
  time = t;
  trace = tr;
}
