#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <TFile.h>
#include <TNamed.h>
#include <TROOT.h>
#include <TTree.h>
#include <vector>
#include "cal_info_t.h"
#include "cal_tools.h"
#include "debug.h"
#include "detectors/detector_t.h"
#include "det_tools.h"
#include "event/event_t.h"
#include "event/event_utils.h"
#include "hit.h"
#include "id.h"
#include "sources.h"

typedef struct {
  bool single;
  bool batch;
  bool dets;
  bool cal;
  char filein[100];
  char fileout[100];
  char filesrc[100];
  char filedet[100];
  char filecal[100];
} gParameters;

int getArgs(gParameters& params, int argc,char **argv);

/*
 * Options specified on the command line
 * -e <filename> -- a single .evt input file
 * -b <filename> -- file containing list of input files
 * -o <filename> -- output root file
 * -s <filename> -- source configuration file
 * -d <filename> -- detector configuration file
 * -c <filename> -- calibration file
 */
bool EVT_DEBUG = false;
std::map<uint64_t, sourcetype_t> sources;

int main (int argc, char *argv[]) {
  FILE *flist = NULL; // File containing list of input filenames
  FILE *fp    = NULL; // Input files
  FILE *fsrc  = NULL; // Sourceid configuration file
  FILE *fdet  = NULL; // Detector configuration file
  FILE *fcal  = NULL; // Calibration file
  gParameters gParams={false, false, false, false, "inputs.dat", "out.root", "sources.cfg", "dets.cfg", "calib.dat"};
  char line[256];

  std::vector<detector_t*> allDets;
  std::map<daq_id_t, det_id_t> chanMap;
  std::map<daq_id_t, cal_info_t> calMap;


  // Read in command line arguments
  if (getArgs(gParams,argc,argv) != 0) {
    fprintf(stderr, "ERROR: Invalid input!\n");
    exit(-1);
  }


  // Populate list of filenames
  std::vector<std::string> fnames;
  if (gParams.single == false && gParams.batch == false) {
    fprintf(stderr,"ERROR: No .evt or batch file specified.\n");
    exit(-1);
  }
  if (gParams.batch == true) {
    // Make sure flist is good
    if ((flist=fopen(gParams.filein, "r")) == NULL) {
      fprintf(stderr, "ERROR: Can't open input file '%s'!\n", gParams.filein);
      exit(-2);
    }
    while (fgets(line, 256, flist)) {
      for (size_t i = 0; i < 256; ++i) {
        if ((int)(line[i]) == '\n') {
          line[i] = '\0';
        }
      }
      fnames.push_back(std::string(line));
    }
    fclose(flist);
  }
  if (gParams.single == true) {
    fnames.push_back(std::string(gParams.filein));
  }


  // Read in sourceids
  if ((fsrc = fopen(gParams.filesrc, "r")) == NULL) {
    fprintf(stderr, "ERROR: Can't open config file '%s'!\n", gParams.filesrc);
    exit(-2);
  }
  while (!feof(fsrc)) {
    uint64_t id;
    char type[100];
    fscanf(fsrc, "%ld\t%s", &id, type);
    if (feof(fsrc)) {
      break;
    }
    if (strcmp(type, "asics") == 0) {
      sources[id] = ASICS;
    } else if (strcmp(type, "ddas") == 0) {
      sources[id] = DDAS;
    } else {
      fprintf(stderr, "Unknown sourceid in configuration file.\n");
    }
  }
  fclose(fsrc);


  // Open output root file
  hits_t all;
  TFile *outfile = new TFile(gParams.fileout, "RECREATE");
  TNamed *file_info = new TNamed("file_info", "");
  TTree *tree = new TTree("tree","Events");
  std::string file_info_str;

  tree->Branch("size",     all.get_len_ptr(),      "size/l");
  tree->Branch("sourceid", all.get_sourceid_ptr(), "sourceid[size]/l");
  tree->Branch("crateid",  all.get_crateid_ptr(),  "crateid[size]/l");
  tree->Branch("slotid",   all.get_slotid_ptr(),   "slotid[size]/l");
  tree->Branch("chanid",   all.get_chanid_ptr(),   "chanid[size]/l");
  tree->Branch("detid",    all.get_detid_ptr(),    "detid[size]/l");
  tree->Branch("detch",    all.get_detch_ptr(),    "detch[size]/l");
  tree->Branch("rawval",   all.get_rawval_ptr(),   "rawval[size]/l");
  tree->Branch("value",    all.get_value_ptr(),    "value[size]/l");
  tree->Branch("energy",   all.get_energy_ptr(),   "energy[size]/D");
  tree->Branch("time",     all.get_time_ptr(),     "time[size]/D");



  // Read in detector config file
  if (gParams.dets) {
    if ((fdet = fopen(gParams.filedet, "r")) == NULL) {
      fprintf(stderr, "ERROR: Can't open detector file '%s'!\n", gParams.filedet);
      exit(-2);
    }
    get_allDets(fdet, allDets, chanMap);
    fclose(fdet);
  }


  // Read in calibration file
  if (gParams.cal) {
    if ((fcal = fopen(gParams.filecal, "r")) == NULL) {
      fprintf(stderr, "ERROR: Can't open calibration file '%s'!\n", gParams.filecal);
      exit(-2);
    }
    get_cal(fcal, calMap);

    fclose(fcal);
  }




  // Loop over input files
  for (size_t fnum = 0; fnum < fnames.size(); ++fnum) {
    printf("Reading %s\n", fnames[fnum].c_str());

    if ((fp=fopen(fnames[fnum].c_str(),"r"))==NULL) {
      fprintf(stderr,"Can't open evtfile %s, moving to next file\n", fnames[fnum].c_str());
      continue;
    }
    file_info_str += fnames[fnum] + "\n";

    // Go to the end of file to note where it is, then go back to the beginning
    // This is for printing the progress
    fseek(fp, 0, SEEK_END);
    size_t end = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    while(!feof(fp)) {
      // Print progress through file
      if (!EVT_DEBUG) {
        printf("%5.2f%%\r", 100.*((double)ftell(fp)/(double)end));
      }

      std::vector<uint8_t> buf;
      size_t index = 0;

      // Read in the size and undo the read (since the size is part of the buffer)
      buf.resize(4);
      fread(buf.data(), sizeof(uint8_t), 4, fp);
      uint64_t size = getBufWord(buf, index, 4);
      // The file might end with some 0s, so don't rewind if that's the case
      if (size != 0) {
        fseek(fp, -4, SEEK_CUR);
      }

      // Read in the whole buffer
      buf.resize(size);
      fread(buf.data(), sizeof(uint8_t), buf.size(), fp);

      if (EVT_DEBUG) {
        printf("%ld\n", buf.size());
        for (size_t buf_i = 0; buf_i < buf.size(); ++ buf_i) {
          printf("%2.2x ", buf[buf_i]);
        }
        printf("\n\n");
      }

      // Parse Buffer
      event_t ev(buf);

      // If we change size, the pointers might change,
      // so we need to re-set the branch addresses
      if (ev.hits.size() > all.get_maxMult()) {
        all.reserve(ev.hits.size());
        tree->SetBranchAddress("sourceid",(ULong64_t*) all.get_sourceid_ptr());
        tree->SetBranchAddress("crateid", (ULong64_t*) all.get_crateid_ptr());
        tree->SetBranchAddress("slotid",  (ULong64_t*) all.get_slotid_ptr());
        tree->SetBranchAddress("chanid",  (ULong64_t*) all.get_chanid_ptr());
        tree->SetBranchAddress("detid",   (ULong64_t*) all.get_detid_ptr());
        tree->SetBranchAddress("detch",   (ULong64_t*) all.get_detch_ptr());
        tree->SetBranchAddress("rawval",  (ULong64_t*) all.get_rawval_ptr());
        tree->SetBranchAddress("value",   (ULong64_t*) all.get_value_ptr());
        tree->SetBranchAddress("energy",  all.get_energy_ptr());
        tree->SetBranchAddress("time",    all.get_time_ptr());
      }
      // TODO: there's gotta be a better way to do this
      all = ev.hits;

      // Make detectors
      if (gParams.dets) {
        make_dets(all, allDets, chanMap);
      }


      // Calibrate
      if (gParams.cal) {
        calibrate(all, calMap);
      }



      // Don't fill the tree with an empty event
      // This can happen if:
      // 1. It wasn't a data event (start/stop run, scalers, etc.)
      // 2. It was a data event, but no actual data was in it
      if (all.size() > 0)
      {
        tree->Fill();
      }

      // Make sure you don't leave junk in the detector data
      all.Reset();
    }

    fclose(fp);
    printf(" Done   \n");
  }

  // Write and close ROOT file
  file_info->SetTitle(file_info_str.c_str());
  file_info->Write(0, TObject::kOverwrite);
  outfile->Write(0, TObject::kOverwrite);
  outfile->Close();

  return 0;
}

int getArgs(gParameters& params, int argc, char**argv) {
  char opt;
  int good = 0;

  while ((opt = (char)getopt(argc, argv, "e:b:o:s:d:c:")) != EOF) {
    switch (opt) {
      case 'e': params.single = true;
                params.batch = false;
                strcpy(params.filein, optarg);
                break;
      case 'b': params.batch = true;
                params.single = false;
                strcpy(params.filein, optarg);
                break;
      case 'o': strcpy(params.fileout, optarg);
                break;
      case 's': strcpy(params.filesrc, optarg);
                break;
      case 'd': params.dets = true;
                strcpy(params.filedet, optarg);
                break;
      case 'c': params.cal = true;
                strcpy(params.filecal, optarg);
                break;
    }
  }

  return (good);
}
