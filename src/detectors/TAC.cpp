#include <stdint.h>
#include <string>
#include "../id.h"
#include "TAC.h"

TAC_t::TAC_t(std::string n, daq_id_t id): detector_t(1) {
  name = n;
  startId = id;
}

bool TAC_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if (id.slotid == startId.slotid) {
        if (id.chanid == startId.chanid) {
          return true;
        }
      }
    }
  }
  return false;
}

uint64_t TAC_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return 0;
}

daq_id_t TAC_t::det_to_daq(uint64_t ch __attribute__((unused))) {
  daq_id_t id = startId;
  return id;
}
