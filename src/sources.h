#ifndef _SOURCES_H_
#define _SOURCES_H_

#include <map>
#include <stdint.h>

enum sourcetype_t {ASICS, DDAS};

extern std::map<uint64_t, sourcetype_t> sources;

#endif
