# evt2root
`evt2root` is a tool that converts from the [NSCL DAQ] ringbuffer format to a
[ROOT] file.

[NSCL DAQ]: http://docs.nscl.msu.edu/daq/index.php
[ROOT]: https://root.cern.ch/

## Usage
```
evt2root (-e evt_file | -b batch_file) [-o output_file] [-s sourceid_file] [-d detector_file] [-c calibration_file]
```

* `-e evt_file`: An nscldaq-11.1 ringbuffer `.evt` file.
* `-b batch_file`: A text file containing a list of nscldaq-11.1 ringbuffer `.evt` files, separated by newlines.
* `-o output_file`: A ROOT file, which will be created, or reacreated if it already exists [default: `out.root`].
* `-s sourceid_file`: A text file containing the sourceid configuration [default: `sources.cfg`]. If the file is not found, the program will exit.
* `-d detector_file`: A text file containing the detector configuration. If this flag is not supplied, the detector configuration is not performed.
* `-c calibration_file`: A text file containing the calibration information. If this flag is not supplied, the calibration is not performed.

### output\_file
The ouput ROOT file contains a `TTree` named `tree` with the following branches:

* `sourceid`: Part of the DAQ ID (`uint64_t`)
* `crateid`: Part of the DAQ ID (`uint64_t`)
* `slotid`: Part of the DAQ ID (`uint64_t`)
* `chanid`: Part of the DAQ ID (`uint64_t`)
* `detid`: Part of the detector ID, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is 0. (`uint64_t`)
* `detch`: Part of the detector ID, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is 0.(`uint64_t`)
* `rawval`: Energy channel, accoridng to the DAQ. (`uint64_t`)
* `value`: Energy channel corrected for polarity, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is the same as `rawval`. (`uint64_t`)
* `energy`: Calibrated energy, if a calibration is provided. Otherwise, it is the same as `value`. (`double`)
* `time`: Time of the hit, according to the DAQ. (`double`)

It also contains a `TNamed` named `det_info` which contains the detector configuration and a `TNamed` named `file_info` which contains the `.evt` files that were used to create it.

### sourceid\_file
Format:
```
sourceid	source_type
```

The following source types are currently supported:

* `asics`
* `ddas`

### detector\_file
Format:
```
det_type	det_name	sourceid	crate_id	slotid	chanid
```

The following detector types are supported:

* `BB10_F`
* `BB15_B`
* `BB15_F`
* `HABANERO`
* `HAGRID`
* `PSIC_E`
* `PSIC_XY`
* `TAC`
* `YY1_F`

### calibration\_file
Format:
```
sourceid	crate_id	slotid	chanid	offset	slope
```

## Other Tools
These are not yet ready

### make\_dets
### calibrate
